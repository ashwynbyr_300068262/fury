import React from 'react'
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [
    {
        title: "CMS",
        path: "/cms",
        icon: <IoIcons.IoIosPaper/>,
        cName: "api-doc"
    },
    {
        title: "Partner Experience",
        path: "/pe",
        icon: <IoIcons.IoIosPaper/>,
        cName: "api-doc"
    },
    {
        title: "WMS",
        path: "/wms",
        icon: <IoIcons.IoIosPaper/>,
        cName: "api-doc"
    }
]