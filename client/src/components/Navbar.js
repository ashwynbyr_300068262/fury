import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import {SidebarData} from './SidebarData';
import './Navbar.css'
import {IconContext} from 'react-icons';

function Navbar() {
    const [sidebar, setSidebar] = useState(false)
    const showSidebar = () => setSidebar(!sidebar)
    return (
        <>
        <IconContext.Provider value = {{color: '#fff'}}>
            <div className="navbar">
                <Link to="#" className="dashboard-logo">
                    <h2>Fury</h2>
                </Link>
                <div className="navbar-heading">
                    <h2>API Documentation</h2>
                </div>
                
            </div>
            <nav className={sidebar ? 'nav-menu active': 'nav-menu' }>
                <ul className="nav-menu-items">
                    {/* <li className="navbar-toggle" onClick={showSidebar}>
                        <Link to="#" className="menu-bars">
                            <AiIcons.AiOutlineClose/>
                        </Link>
                    </li> */}
                    {SidebarData.map((item, index) => {
                        return (
                            <li key={index} className='nav-text'>
                                <Link to={item.path}>
                                    <span className='icon'>{item.icon}</span>
                                    <span>{item.title}</span>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </nav>
        </IconContext.Provider>
        
        </>
    )
}

export default Navbar
