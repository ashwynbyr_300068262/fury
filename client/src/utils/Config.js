
const cms = [
    {
        'name': "Sizingengine",
        'jsonDoc': "http://sizingengine.stage.myntra.com/myntra-sizingengine-service/api-docs/swagger.json",
        'ui': "http://sizingengine.stage.myntra.com/myntra-sizingengine-service/api-docs/api-docs?url=/myntra-sizingengine-service/api-docs/swagger.json"
    },
    {
        'name': 'DAM Service',
        'jsonDoc': 'http://dam.stage.myntra.com/myntra-sizingengine-service/api-docs/swagger.json',
        'ui': 'http://dam.stage.myntra.com/myntra-asset-service/api-docs/api-docs?url=/myntra-asset-service/api-docs/swagger.json'
    }
]

const config = {
    cms
}

export default config;