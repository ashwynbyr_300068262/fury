import React from 'react'
import config from '../utils/Config'
import './Common.css'

const { cms } = config;
function Cms() {
    return (
        <div className='api-doc'>
            <ul className='service-list'> 
                {cms.map((item, index) => {
                    return (
                        <li key={index} className='service-block'>
                            <h4>{item.name}</h4>
                            <div className="service-block-item">
                                <tr>
                                    <td className='info-key'>Json Doc:</td>
                                    <td>
                                        <a href={item.jsonDoc} target="_blank" >
                                            <span>{item.jsonDoc}</span>
                                        </a>
                                    </td> 
                                </tr>
                                
                            </div>
                            <div className="service-block-item">
                                <tr>
                                    <td className='info-key'>Swagger UI: </td>
                                    <td>
                                        <a href={item.ui} target="_blank" className="service-block-item">
                                            <span>{item.ui}</span>
                                        </a>
                                    </td>
                                </tr>
                                
                            </div>
                            
                        </li>
                    
                    )
                })}
            </ul>
        </div>
        
        
        // <div className='api-doc'>
        //     <span>Sizingengine:</span>
        //     <Link to="#">Swagger UI</Link>   

        // </div>
        
    )
}

export default Cms
